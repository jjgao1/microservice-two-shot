import React from 'react'

function ShoesList(props) {
    return (<table className="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Model Name</th>
      </tr>
    </thead>
    <tbody>
      {props.shoe?.map(shoe => {
        return (
          <tr key={shoe.href}>
            <td>{ shoe.id }</td>
            <td>{ shoe.model_name }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
);
}

export default ShoesList;
