import React, { useEffect, useState } from "react";

function ShoeForm() {
    const [bins, setBins] = useState([])
    const [manufacturerName, setManufacturerName] = useState('')
    const [email, setEmail] = useState('')
    const [bin, setBin] = useState('')
    const [created, setCreated] = useState(false);



    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
    }

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.bin = bin;
        data.manufacturerName = manufacturerName;
        data.email = email;


        console.log(data);

        const shoeUrl = 'http://localhost:3000/api/bins/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {

            setManufacturerName('');
            setBin('');
            setEmail('');
            setCreated(true);


        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:3000/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (bins.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (created) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }