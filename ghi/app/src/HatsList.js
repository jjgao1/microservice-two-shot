import React from "react";

function HatsList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Style Name</th>
                </tr>
            </thead>
            <tbody>
                {props.hats?.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.id }</td>
                            <td>{ hat.style_name }</td>
                        </tr>
                    );
                })}
            </tbody>
      </table>
    );
}

export default HatsList;
