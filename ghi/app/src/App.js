import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';


function App(props) {
  if (props.hats === undefined) {
    return null;
  }
import React from 'react';
import ShoesList from './ShoesList';

function App(props) {
  const [data, setData] = React.useState([])


  // if (props.shoes === undefined) {
  //   return null;
  // }
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element = {<MainPage />}/>
          <Route path="hats">
              <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="/hats" element = {<HatsList />}/>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList/>} />
          <Route path="ShoesList">
            <Route path="new" element={<ShoesList shoes={props.shoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
