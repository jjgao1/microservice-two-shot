# Generated by Django 4.0.3 on 2023-04-20 14:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_binvo_rename_url_shoe_image_url_alter_shoe_bin'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Shoe',
        ),
    ]
