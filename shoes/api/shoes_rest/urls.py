from django.urls import path
from .views import api_list_shoes, shoe_detail

urlpatterns = [
    path('shoes/', api_list_shoes, name='api_shoes'),
    path('shoes/<int:pk>/', shoe_detail, name='shoe_detail'),
]