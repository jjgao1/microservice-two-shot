from django.db import models
from django.urls import reverse

# Create your models here.


class BinVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)


class Shoe(models.Model):
    """
    The Shoe model represents a pair of shoes that exists in the wardrobe
    """
    manufacturer_name = models.CharField(max_length=20)
    model_name = models.CharField(max_length=20)
    color = models.CharField(max_length=20)
    url = models.URLField(max_length=200)
    bin = models.ForeignKey(BinVO, related_name="bins", on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("shoe_detail", kwargs={
            "pk": self.pk,
        }) 
