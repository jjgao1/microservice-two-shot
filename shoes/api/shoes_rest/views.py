from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .models import Shoe, BinVO
import json

from common.json import ModelEncoder


# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "id",]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "color",
        "image_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder,
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            bin_id = content['bin_id']
            bin = BinVO.objects.get(id=bin_id)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(["GET","PUT", "DELETE"])
def shoe_detail(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:   #PUT
        try:
            content = json.loads(request.body)
            bin = Shoe.objects.get(id=pk)

            props = ["closet_name", "bin_number", "bin_size"]
            for prop in props:
                if prop in content:
                    setattr(bin, prop, content[prop])
            shoe.save()
            return JsonResponse(
                bin,
                encoder=ShoeListEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

