# Wardrobify

THIS IS A TEST NNNNNNNN

Team:

* Person 1 - Fermin Santiago - Shoes
* Person 2 - Junjie - Hats

## Design

Backend:
3 microservices - Hats, Wardrobe, Shoes

Frontend:
React
* Person 2 - Which microservice?

## Design


## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

There will be two models: BinVO & Shoe
Wardrobe will pull data from BinVO as and API call and continue polling
React will host the data and display it for the user

## Hats microservice

There would be a Hat model and LocationVO model within the Hats microservice. The Wardrobe microservice would poll information from the Hats microservice using the LocationVO model.
