from django.db import models
from django.urls import reverse

# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Hats(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    url = models.URLField(max_length=200)
    location = models.ForeignKey(LocationVO, related_name="hats", on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("api_hat_detail", kwargs={
            "pk":self.pk,
        })
